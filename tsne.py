import numpy as np
from sklearn.manifold import TSNE
'''
input files: ./data/medscape.tok.sg.d50.w*
output directory: ./data/tsne/
'''
	
def do_TSNE(filenames, format):
	dim = 2 # Set num of dimensions to reduce to	
	model = TSNE(n_components=dim, random_state=0)
	np.set_printoptions(suppress=True)
	r = range(1,51)	
	for f in filenames:
		values = np.loadtxt('../data/'+f, delimiter=' ', usecols=(r))
		my_tsne =  model.fit_transform(values)
    # save as "npy" or as "txt"
		if format == "npy":
			np.save('tsne/tsne'+str(dim)+f, my_tsne)
		else:
			tsne_str = str(my_tsne.tolist()).replace("], [", "\n").replace("[[", "").replace("]]", "")
			new = open('../data/tsne/tsne-'+str(dim)+f+'.txt', 'w+')
			new.write(tsne_str)
			new.close()
	return


filenames = []
fn0 = "medscape.tok.sg.d50.w"
for n in range(1,8):
	filenames.append(fn0+str(n)+".txt")

do_TSNE(filenames, "txt")


